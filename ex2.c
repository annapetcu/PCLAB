#include <stdio.h>
int main(){
	unsigned int n;
	unsigned int mask = 128;

	printf("n: ");
	scanf("%d", &n);

	while (mask > 0){
		if((n & mask) == 0)	
			printf("0");
		else printf("1");
		printf("\n");
		mask = mask >> 1;
	}

	return 0;
}
